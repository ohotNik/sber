package ru.sber.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sber.test.domain.Client;
import ru.sber.test.repository.ClientRepo;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

/**
 * @since 27.04.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
public class OperationsServiceTest {

  @Autowired
  private OperationsService operationsService;

  @Autowired
  private ClientRepo clientRepo;


  @Before
  public void before() {
    clientRepo.save(new Client(1L, "client1", 500L));
    clientRepo.save(new Client(2L, "client2", 5L));
    clientRepo.save(new Client(3L, "client3", 10000L));
  }

  /*

  DEPOSIT

   */

  @Test(expected = IllegalArgumentException.class)
  public void testExcAdd1() {
    operationsService.add(4, 10);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testExcAdd2() {
    operationsService.add(1, -10);
  }

  @Test
  public void testAdd() {
    Client one = clientRepo.findOne(1L);
    assert one != null;
    operationsService.add(1, 33);
    Client two = clientRepo.findOne(1L);
    assert two != null;
    Assert.assertTrue(one.getAmount() < two.getAmount());
  }

  /*

  WITHDRAW

   */

  @Test(expected = IllegalArgumentException.class)
  public void testExcDelete1() {
    operationsService.delete(4, 10);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testExcDelete2() {
    operationsService.delete(1, -10);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testExcDelete3() {
    operationsService.delete(1, 10000);
  }

  @Test
  public void testDelete() {
    Client one = clientRepo.findOne(2L);
    assert one != null;
    operationsService.delete(2, 1);
    Client two = clientRepo.findOne(2L);
    assert two != null;
    Assert.assertTrue(one.getAmount() - two.getAmount() == 1);

    operationsService.delete(2, two.getAmount());
    two = clientRepo.findOne(2L);
    Assert.assertTrue(two.getAmount() == 0);
  }



  /*

  TRANSFER

   */

  @Test
  public void testTransfer1() {
    operationsService.transfer(1,2, 500);
    final Client one = clientRepo.findOne(1L);
    final Client two = clientRepo.findOne(2L);

    Assert.assertTrue(one.getAmount() == 0);
    Assert.assertTrue(two.getAmount() == 505);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTransfer2() {
    try {
      operationsService.transfer(1, 5, 100);
    } finally {
      final Client client = clientRepo.findOne(1L);
      Assert.assertTrue(client.getAmount() == 500);
    }
  }



}