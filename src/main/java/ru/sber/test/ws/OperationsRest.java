package ru.sber.test.ws;

import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.test.service.OperationsService;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.security.SecureRandom;

/**
 * @since 27.04.2017.
 */
@RestController
@NoArgsConstructor
public class OperationsRest {

  @Inject
  private OperationsService operationsService;

  private int defRetry = 5;
  private final SecureRandom secureRandom = new SecureRandom();

  @RequestMapping("/deposit")
  public void deposit(@RequestParam("clientId") final long clientId, @RequestParam("summ") final long summ) throws InterruptedException {
    int i = defRetry;
    do {
      try {
        operationsService.add(clientId, summ);
        break;
      } catch (PersistenceException ex) {
        if (--i < 0)
          throw new IllegalStateException("Server error. Try one more time.");
        Thread.sleep(100 + secureRandom.nextInt(200));
      }
    } while (true);
  }

  @RequestMapping("/withdraw")
  public void withdraw(@RequestParam("clientId") final long clientId, @RequestParam("summ") final long summ) throws InterruptedException {
    int i = defRetry;
    do {
      try {
        operationsService.delete(clientId, summ);
        break;
      } catch (PersistenceException ex) {
        if (--i < 0)
          throw new IllegalStateException("Server error. Try one more time.");
        Thread.sleep(100 + secureRandom.nextInt(200));
      }
    } while (true);
  }

  @RequestMapping("/transfer")
  public void transfer(
      @RequestParam("clientFrom") final long clientFrom,
      @RequestParam("clientTo") final long clientTo,
      @RequestParam("summ") final long summ
  ) throws InterruptedException {
    int i = defRetry;
    do {
      try {
        operationsService.transfer(clientFrom, clientTo, summ);
        break;
      } catch (PersistenceException ex) {
        if (--i < 0)
          throw new IllegalStateException("Server error. Try one more time.");
        Thread.sleep(100 + secureRandom.nextInt(200));
      }
    } while (true);
  }

}
