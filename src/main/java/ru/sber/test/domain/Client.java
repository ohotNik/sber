package ru.sber.test.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.io.Serializable;

/**
 * @since 27.04.2017.
 */
@Entity
@DynamicUpdate
@Getter
@Setter
@NoArgsConstructor
public class Client implements Serializable {

  private static final long serialVersionUID = 3375173189692384564L;

  public Client(Long id, String clientName, Long amount) {
    this.id = id;
    this.clientName = clientName;
    this.amount = amount;
  }

  @Id
  private Long id;

  private String clientName;

  private Long amount;

  @Version
  private Long version;

}
