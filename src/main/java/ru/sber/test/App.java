package ru.sber.test;

import com.vaadin.spring.annotation.EnableVaadin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 27.04.2017.
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableVaadin
@ComponentScan(basePackages = "ru.sber.test")
@EnableJpaRepositories(basePackages = "ru.sber.test.repository")
public class App {

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }


}
