package ru.sber.test.ui;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sber.test.domain.Client;
import ru.sber.test.repository.ClientRepo;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringView(name = UsersView.VIEW_NAME)
public class UsersView extends VerticalLayout implements View {
  static final String VIEW_NAME = "";
  private static final long serialVersionUID = -6490357425570135050L;

  @Autowired
  private ClientRepo clientRepo;

  @PostConstruct
  void init() {
    setSizeFull();
    setMargin(true);

    final List<Client> data = clientRepo.findAll();

    BeanItemContainer<Client> beans =
        new BeanItemContainer<>(Client.class);
    beans.addAll(data);

    Table table = new Table("Пользователи", beans);

    table.setVisibleColumns("id", "clientName", "amount");
    table.setColumnHeaders("Id", "Клиент", "Счет");

    table.setPageLength(10);

    addComponent(table);


  }

  @Override
  public void enter(ViewChangeListener.ViewChangeEvent event) {
    // the view is constructed in the init() method()
  }
}