package ru.sber.test.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI(path = "/")
@DesignRoot
public class VaadinUI extends UI {

  private static final long serialVersionUID = 7728288529790832140L;
  @Autowired
  private SpringViewProvider viewProvider;

  @Override
  protected void init(VaadinRequest request) {
    VerticalLayout mainLayout = new VerticalLayout();
    mainLayout.setSizeFull();
    mainLayout.setMargin(true);
    mainLayout.setSpacing(true);
    MenuBar menuBar = new MenuBar();
    menuBar.addItem("Пользователи",
            (MenuBar.Command) selectedItem -> getUI().getNavigator().navigateTo(""));
    mainLayout.addComponent(menuBar);
    setContent(mainLayout);

    System.out.println();

    final Panel viewContainer = new Panel();
    viewContainer.setSizeFull();
    mainLayout.addComponent(viewContainer);
    mainLayout.setExpandRatio(viewContainer, 1.0f);

    Navigator navigator = new Navigator(this, viewContainer);
    navigator.addProvider(viewProvider);

  }
}