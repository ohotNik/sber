package ru.sber.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import ru.sber.test.domain.Client;

import static javax.persistence.LockModeType.OPTIMISTIC;

/**
 * @since 27.04.2017.
 */
public interface ClientRepo extends JpaRepository<Client, Long> {

  @Lock(value = OPTIMISTIC)
  Client findOneById(final Long id);

}
