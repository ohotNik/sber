package ru.sber.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.test.domain.Client;
import ru.sber.test.repository.ClientRepo;

import javax.transaction.Transactional;

/**
 * @since 27.04.2017.
 */
@Service
public class OperationsService {


  @Autowired
  private ClientRepo clientRepo;

  @Transactional
  public void add(final long clientId, final long summ) {

    if (summ <= 0)
      throw new IllegalArgumentException("Summ has to be natural.");

    final Client client = clientRepo.findOneById(clientId);
    if (client == null)
      throw new IllegalArgumentException("No such user");


    client.setAmount(client.getAmount() + summ);
    clientRepo.save(client);
  }

  @Transactional
  public void delete(final long clientId, final long summ) {

    if (summ <= 0)
      throw new IllegalArgumentException("Summ has to be natural.");

    final Client client = clientRepo.findOneById(clientId);
    if (client == null)
      throw new IllegalArgumentException("No such user");

    if (client.getAmount() < summ)
      throw new IllegalArgumentException("Unable to withdraw the summ.");


    client.setAmount(client.getAmount() - summ);
    clientRepo.save(client);
  }

  @Transactional
  public void transfer(final long clientFrom, final long clientTo, final long summ) {
    if (clientRepo.findOne(clientTo) == null)
      throw new IllegalArgumentException("Receiver does not exist..");
    delete(clientFrom, summ);
    add(clientTo, summ);
  }

}
